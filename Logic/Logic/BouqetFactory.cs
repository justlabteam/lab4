﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flowers;

namespace Logic
{
    public static class BouqetFactory
    {
        public static Bouquet CreateBouqet()
        {
            Bouquet bouquet = new Bouquet();                            // Создаем букет и заполняем его экземплярами цветов.
            ArtificialFlower artificialFlower = (ArtificialFlower)FlowersFactory.CreateFlower("Artificial"); // Для создания экземпляров цветов используем класс FlowerFactory
            artificialFlower.Name = "Artificial Blue Rose";
            artificialFlower.Color = "Blue";
            artificialFlower.Price = 17.5;
            artificialFlower.Material = "Plastic";
            bouquet.AddFlower(artificialFlower);
            NaturalFlower naturalFlower = (NaturalFlower)FlowersFactory.CreateFlower("Natural");
            naturalFlower.Name = "Violets";
            naturalFlower.Color = "Purple";
            naturalFlower.Price = 20;
            naturalFlower.Freshness = 3;
            bouquet.AddFlower(naturalFlower);
            Rose rose = (Rose)FlowersFactory.CreateFlower("Rose");
            rose.Name = "Red roses";
            rose.Color = "Red";
            rose.Price = 50;
            rose.Freshness = 5;
            rose.SpikeLength = 0.15;
            bouquet.AddFlower(rose);
            Lily lily = (Lily)FlowersFactory.CreateFlower("Lily");
            lily.Name = "White lily";
            lily.Color = "White";
            lily.Price = 35;
            lily.Freshness = 4;
            lily.BulbDiameter = 3;
            bouquet.AddFlower(lily);
            return bouquet;
        }
    }
}
