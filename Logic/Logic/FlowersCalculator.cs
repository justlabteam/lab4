﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flowers;

namespace Logic
{
    public static class FlowersCalculator
    {
        public static double GetTotalPrice(Bouquet bouquet)
        {
            double totalPrice = 0;
            List<AbstractFlower> listOfFlowers = bouquet.ListOfFlowers;
            foreach (AbstractFlower flower in listOfFlowers)        //Суммируем сумму для каждого цветка
            {
                totalPrice += flower.Price;
            }
            return totalPrice;
        }
    }
}
