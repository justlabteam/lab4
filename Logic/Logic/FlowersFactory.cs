﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flowers;

namespace Logic
{
    public class FlowersFactory
    {
        public static AbstractFlower CreateFlower(String type) //В качетсве аргумента передаем тип - класс,    
        {                                                      //к которому должен принадлежать данный цветок.
            AbstractFlower Flower = null;                      //В соответствие с этим аргументов, создаем 
            if (type.Equals("Artificial"))                     //экземпляр класса.
                Flower = new ArtificialFlower();
            if (type.Equals("Natural"))
                Flower = new NaturalFlower();
            if (type.Equals("Rose"))
                Flower = new Rose();
            if (type.Equals("Lily"))
                Flower = new Lily();
            return Flower;
        }
    }
}
