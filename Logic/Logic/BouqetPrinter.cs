﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flowers;

namespace Logic
{
    public static class BouqetPrinter
    {
        public static void Print(Bouquet bouquet)
        {
            List<AbstractFlower> listOfFlowers = bouquet.ListOfFlowers;
            foreach (AbstractFlower flower in listOfFlowers)
            {
                if (flower != null)                                             // Определяем класс, к которому 
                {                                                              // принадлежит цветок, и выводим всю информацию.
                    Console.WriteLine("Name: " + flower.Name);
                    Console.WriteLine("Color: " + flower.Color);
                    Console.WriteLine("Price: " + flower.Price);
                    if (flower is ArtificialFlower)
                        Console.WriteLine("Material: " + (flower as ArtificialFlower).Material);
                    if (flower is NaturalFlower)
                    {
                        Console.WriteLine("Freshness: " + (flower as NaturalFlower).Freshness);
                        if (flower is Rose)
                            Console.WriteLine("Spike Length: " + (flower as Rose).SpikeLength);
                        if (flower is Lily)
                            Console.WriteLine("Bulb Diameter: " + (flower as Lily).BulbDiameter);
                    }
                    Console.WriteLine("\n");
                }
            }
        }
    }
}
